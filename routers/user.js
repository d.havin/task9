const express = require("express");
const userRouter = express.Router();
const userController = require("../controllers/userController");

userRouter.post("/create",  userController.userCreate);
userRouter.get("/:id", userController.getUserInfo);
userRouter.delete("/:id", userController.deleteUser);
userRouter.put("/:id", userController.updateUser);

module.exports = userRouter;
