const express = require("express");
const authRouter = express.Router();
const authController = require("../auth.js");

authRouter.post("/", authController.checkVisitor);

module.exports = authRouter;