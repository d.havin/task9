const express = require("express");
const adminRouter = express.Router();
const adminController = require("../controllers/adminController");

adminRouter.post("/create", adminController.adminCreate);
adminRouter.get("/:id", adminController.getAdminInfo);
adminRouter.delete("/:id", adminController.deleteAdmin);
adminRouter.put("/:id", adminController.updateAdmin);

module.exports = adminRouter;

