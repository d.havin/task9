const express = require("express");
const roomRouter = express.Router();
const authentification = require("../auth");
const roomController = require("../controllers/roomController");

roomRouter.post("/create", authentification.checkAccess, roomController.roomCreate);
roomRouter.delete("/:id", authentification.checkAccess, roomController.roomDelete);
roomRouter.put("/:id", authentification.checkAccess, roomController.editRoom);
roomRouter.get("/roomList", authentification.checkAccess, roomController.getRoomsList);
roomRouter.get("/:id", authentification.checkAccess, roomController.roomCreate);

module.exports = roomRouter;