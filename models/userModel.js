const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const auto = require('mongoose-autopopulate');

const userScheme = new Schema({

    userName: { type: String,
        min: 3,
        max: 20,
        default: "NoName"
    },
    userAge: {
        type: Number,
        required: true,
        min: 18,
        max: 100,
        default: 18
    }, 
    login: { type: String,
        required: true,
        minlength :4
    },
    role: {
        type: String,
        required: true,
        default: "user"
    },
    password: { type: String,
        required: true,
        minlength :4
    },
    rooms : [{
        type: mongoose.Schema.Types.ObjectId, ref: "Room", autopopulate: true
    }]
},
{versionKey: false },
);
userScheme.plugin(auto);

const User = mongoose.model("User", userScheme);
module.exports = User;
