const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const auto = require('mongoose-autopopulate')

const roomSchema = new Schema({
    number: { type: Number,
        required: true,
        max: 99,
        unique: true
    },
    users: [{type: mongoose.Schema.Types.ObjectId, ref: "User", autopopulate: true}]
    ,
    admin: {type: mongoose.Schema.Types.ObjectId, ref: "Admin", autopopulate: true},
});
roomSchema.plugin(auto);
 
const Room = mongoose.model("Room", roomSchema);
module.exports = Room;
