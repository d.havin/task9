const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const auto = require('mongoose-autopopulate')

const adminScheme = new Schema({
    adminId: { type: Number,
        required: true,
        max : 9999,
    },
    login: { type: String,
        required: true,
        minlength :4
    },
    role: {
        type: String,
        required: true,
        default: "admin"

    },
    password: { type: String,
        required: true,
        minlength :4
    },
    rooms : [{
        type: mongoose.Schema.Types.ObjectId, ref: "Room", autopopulate: true
    }]
},
{versionKey: false },
);
adminScheme.plugin(auto);

const Admin = mongoose.model("Admin", adminScheme);
module.exports = Admin;