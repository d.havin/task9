const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());
const authentification = require("./auth");

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/usersdb", { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false });

const userRouter = require("./routers/user");
const adminRouter = require("./routers/admin");
const fileWorkerRouter = require("./routers/fileWorker");
const authRouter = require("./routers/authentification");
const roomRouter = require("./routers/room")

app.use('/user', userRouter);
app.use('/admin', adminRouter);
app.use('/fileWorker', fileWorkerRouter);
app.use('/login', authRouter);
app.use('/room', authentification.authenticateJWT, roomRouter);



// const fs =  require('fs');
// let emitter = require("./emitter");

// emitter.on("letSpy", function(copiedFile){
//     let readableStream = fs.createReadStream(copiedFile, "utf8");
//     let writeableStream = fs.createWriteStream("stealedCopy.txt");
//     readableStream.pipe(writeableStream);
// });

app.listen(3000); 
