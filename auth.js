const jwt = require('jsonwebtoken');
const auth = require('registry-auth-token');
const Admin = require("./models/adminModel");
const User = require("./models/userModel");
const accessTokenSecret = 'someaccesstokensecret';

module.exports = {
    checkVisitor : async (request, response) => {
        const user = await User.findOne({login: request.body.login, password: request.body.password});
        const admin = await Admin.findOne({login: request.body.login, password: request.body.password});
        if (user) {
            const accessToken = jwt.sign({ login: user.login,  role: user.role, _id: user._id }, accessTokenSecret, { expiresIn: '300m'}); //для удобства
            response.json({accessToken});
        } else if (admin) {
            const accessToken = jwt.sign({ login: admin.login,  role: admin.role, _id: admin._id }, accessTokenSecret, { expiresIn: '300m'});
            response.json({accessToken});
        } else {
            response.send("Неверный login или password");
        }
    },
    authenticateJWT : (request, response, next) => {
        const authHeader = request.headers.authorization;
        if (authHeader) {
            const token = authHeader.split(' ')[1];
            jwt.verify(token, accessTokenSecret, (err, user) => {
                if (err) {
                    return response.sendStatus(403);
                }
                request.user = user;
                next();
            });
        } else {
            response.sendStatus(401)
        }
    },
    checkAccess : (request, response, next) => {
        if (request.user.role !== "admin") {
            return response.status(403).send("Нет прав доступа")
        }
        next();
    }  
}
