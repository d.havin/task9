const Admin = require("../models/adminModel");
const Room = require("../models/roomModel");
const User = require("../models/userModel");

module.exports = {
    roomCreate : async (request, response) => {
        let newRoom = new Room({number: request.body.number});
        let roomAdmin = await Admin.findOne({_id : request.user._id});
        newRoom.admin = request.user._id;
        await newRoom.save();
        roomAdmin.rooms.push(newRoom._id);
        await roomAdmin.save();
        response.send(`Создана комната с номером: ${newRoom.number}`);
    },
    roomDelete : async (request, response) => {
        let room = await Room.findOne({_id: request.params.id});
        let roomAdmin = await Admin.findOne({_id: request.user._id});
        let element = request.params.id;
        let roomArray = roomAdmin.rooms;
        if (room && roomAdmin._id.toString() == room.admin._id.toString()){
            Room.deleteOne({_id: request.params.id}, function(err, result) {
                if (err) return console.log(err);
            });
            roomArray.splice(roomArray.indexOf(element),1);
            await roomAdmin.save();
            response.send(`Комната удалена`);  
        } else {
            response.send(`Не твоя комната - не тебе и удалять`);
        }
    },
    editRoom : async (request, response) => {
        let editableRoom = await Room.findOne({_id: request.params.id});
        let roomEditor = await Admin.findOne({_id: request.user._id});
        if (room && roomEditor._id.toString() == room.admin._id.toString()) {
            Room.updateOne(editableRoom, {number: request.body.number}, {new: true}, function(err, result) {
                if (err) return console.log(err);
            });
            await room.save()
            response.send(`Номер комнаты изменен на ${editableRoom.number}`);
        } else {
            response.send("Номер этой ягодки не для тебя");
        }
    },
    getRoomsList : async (request, response) => {
        if (request.user.role == "admin") {
            let admin = await Admin.findOne({_id: request.user._id});
            response.send(`Вы создали комнаты с номерами ${admin.rooms}`)
        } else {
            let user = await User.findOne({_id: request.user._id});
            response.send(`Номера комнат в которых вы состоите ${user.rooms}`)
        }
    },

    getRoomInfo : async (request, response) => {
        if (request.user.role == "admin") {
            let room = await Room.findOne({_id: request.params.id});
            response.send(`Info about room ${room}`);
        } else {
            let user = await User.findOne({_id: request.user._id});
            let arr = user.rooms;
            let findRoom = request.params.id;
            arr.includes(findRoom)? response.send(`Room info  ${findRoom}`) : response.send(`Не в этот раз`);
        } 
    }
}