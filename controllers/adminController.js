const Admin = require("../models/adminModel.js");

module.exports = {

    adminCreate: (request, response) => {
        Admin.create({ adminId: request.body.adminId, login: request.body.login, password: request.body.password }, function (err, doc) {  //role?
            if (err) return console.log(err);
            response.send("Сохранен объект admin" + doc);
        });
    },

    getAdminInfo: (request, response) => {
        let id = request.params.id
        Admin.find({adminId : id}, function (err, doc) {
            if (err) return console.log(err);
            response.send("Get info about" + doc);
        });
    },
    
    deleteAdmin: (request, response) => {
        let id = request.params.id;
        Admin.findOneAndDelete({adminId : id}, function (err, doc) {
            if (err) return console.log(err);
            response.send(`Удален админ ${doc}`);
        })
    },
    

    updateAdmin: (request, response) => {
        let id = request.params.id;
        Admin.findOneAndUpdate(id, {adminId: request.body.adminId, adminLogin: request.body.login, password: request.body.password}, {new: true}, function(err, admin){
            if(err) return console.log(err);
            response.send("Change info about admin" + admin);
        });
    }
};


