const User = require("../models/userModel.js");

module.exports = {

    userCreate: (request, response) => {
        User.create({userName: request.body.userName, userAge: request.body.userAge, login: request.body.login, password: request.body.password}, function (err, doc) {
            if (err) return console.log(err);
            response.send(doc);
        });
    },

    getUserInfo: (request, response) => {
        let id = request.params.id
        User.find({userId : id}, function (err, docs) { 
            if (err) return console.log(err);
            response.send("Get info about" + docs);
        });
    },

    deleteUser: (request, response) => {
        let id = request.params.id;
        User.findOneAndDelete({userId : id}, function (err, doc) {
            if (err) return console.log(err);
            response.send(`Удален пользователь ${doc}`);
        });
    },

    updateUser: (request, response) => {
        let id = request.params.id;
        User.findOneAndUpdate({userId : id}, {userId: request.body.userId, userName: request.body.userName, userAge: request.body.userAge, login: request.body.login, password: request.body.password}, {new: true}, function(err, user){
            if(err) return console.log(err);
            response.send("Change info about user" + user);
        });
    }
};

